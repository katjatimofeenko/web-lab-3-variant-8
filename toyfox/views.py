from django.shortcuts import render, HttpResponse
from django.template import loader

from .models import *

# Create your views here.

def index(request):
	toys_list = None
	if request.GET.get("search") is None:
		toys_list = Toy.objects.all()
	else:
		toys_list = Toy.objects.filter(title__contains=request.GET.get("search"))

	template = loader.get_template('index.html')
	context = {'toys_list': toys_list,}
	return HttpResponse(template.render(context, request))
