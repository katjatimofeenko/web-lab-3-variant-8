from django.contrib import admin


from .models import *

admin.site.register(ToyType)
admin.site.register(Manufacturer)
admin.site.register(Toy)
