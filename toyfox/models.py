# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class ToyType(models.Model):
	title = models.CharField(max_length = 30, unique = True)

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class Manufacturer(models.Model):
	title = models.CharField(max_length = 30, unique = True)

	def __str__(self):
		return self.title

@python_2_unicode_compatible
class Toy(models.Model):
	title = models.CharField(max_length = 30)
	picUrl = models.CharField(max_length = 100)
	price = models.IntegerField()
	toyType = models.ForeignKey(ToyType, on_delete=models.CASCADE)
	manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE) 

	def __str__(self):		 
		return self.title + " " + str(self.price) + "$ " + self.toyType.title + " " +self.manufacturer.title


