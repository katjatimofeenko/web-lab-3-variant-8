from django.apps import AppConfig


class ToyfoxConfig(AppConfig):
    name = 'toyfox'
