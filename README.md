# README #

Проект содержит начало разработки веб-магазина детских игрушек ToyFox
http://localhost:8000/toyfox/

Создано три сущности: Manufacturer, ToyType, Toy в models.py

Релизовано: отображение всех существующих записей Toy, а также поиск по Toy.title.

Данные были введены через панель администрирования Django http://localhost:8000/admin/: 
User: katja
Password: katja123123

![e9f07a0a4e.jpg](https://bitbucket.org/repo/EGaL5x/images/2687124474-e9f07a0a4e.jpg)